# Changelog

All notable changes to this project will be documented in this file, in reverse chronological order by release.

## 4.0.0 - 2023-03-24

### Added

- Nothing.

### Changed

- Use https://packagist.org/

### Deprecated

- Nothing.

### Removed

- Nothing.

### Fixed

- Nothing.
