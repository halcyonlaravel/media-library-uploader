<?php

namespace HalcyonLaravelBoilerplate\MediaLibraryUploader;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Spatie\MediaLibrary\HasMedia;

class MediaLibraryUploader
{
    /**
     * @var \Spatie\MediaLibrary\HasMedia|\Spatie\MediaLibrary\InteractsWithMedia|\Illuminate\Database\Eloquent\Model
     * @phpstan-ignore-next-line
     */
    private Model $model;

    /**
     * @var string|\Illuminate\Http\UploadedFile
     */
    private $file;

    private ?array $customProperties = null;

    public function __construct(HasMedia $model)
    {
        /** @phpstan-ignore-next-line */
        $this->model = $model;
    }

    public static function setModel(HasMedia $model): self
    {
        return new static($model);
    }

    /**
     * @param  string|\Illuminate\Http\UploadedFile  $file
     */
    public function file($file): self
    {
        $this->file = $file;

        return $this;
    }

    public function customProperties(array $customProperties): self
    {
        $this->customProperties = $customProperties;

        return $this;
    }

    /**
     * @param  string  $collectionName
     * @param  string|null  $defaultPath
     *
     * @return \HalcyonLaravelBoilerplate\MediaLibraryUploader\Models\Media|null
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileCannotBeAdded
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig
     */
    public function execute(
        string $collectionName,
        string $defaultPath = null
    ): ?Models\Media {
        if (app()->environment() == 'testing') {
            return null;
        }

        $info = null;

        if (app()->runningInConsole()) {
            $info = get_class($this->model)." [{$this->model->getKey()}] [$collectionName] Seeding";
            print_r("$info {$this->file} ... \n");
        }

        if (filter_var($this->file, FILTER_VALIDATE_URL) or is_string($this->file)) {
            $fileName = explode('/', $this->file);
            $fileName = $fileName[count($fileName) - 1];
        } elseif ($this->file instanceof UploadedFile) {
            $fileName = $this->file->getClientOriginalName();
        } else {
            $fileName = $this->file;
        }

        $fileName = explode('.', $fileName)[0];
        $fileName = str_replace('%20', ' ', $fileName);
        $fileName = str_replace('-', ' ', $fileName);
        $fileName = str_replace('_', ' ', $fileName);

        $customProperties = array_merge(
            [
                'html_attributes' => [
                    'title' => $fileName,
                    'alt' => $fileName,
                ],
            ],
            $this->customProperties ?: []
        );

        $media = null;

        if (filter_var($this->file, FILTER_VALIDATE_URL)) {
            /** @phpstan-ignore-next-line */
            $media = $this->model
                ->addMediaFromUrl($this->file);
        } elseif (is_string($this->file)) {
            /** @phpstan-ignore-next-line */
            $media = $this->model
                ->copyMedia(
                    is_null($defaultPath) ? test_file_path($this->file) : ($defaultPath.DIRECTORY_SEPARATOR.$this->file)
                );
        } elseif ($this->file instanceof UploadedFile) {
            /** @phpstan-ignore-next-line */
            $media = $this->model
                ->addMedia($this->file)
                ->preservingOriginal();
        }

        /** @var \HalcyonLaravelBoilerplate\MediaLibraryUploader\Models\Media $mediaReturn */
        $mediaReturn = $media
            ->withCustomProperties($customProperties)
            ->toMediaCollection($collectionName);

        touch_timestamp_model($this->model);

        if (app()->runningInConsole()) {
            print_r("$info done! ... \n");
        }

        return $mediaReturn;
    }
}
