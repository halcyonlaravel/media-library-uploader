<?php

namespace HalcyonLaravelBoilerplate\MediaLibraryUploader\Providers\Macros;

use Closure;
use HalcyonLaravelBoilerplate\MediaLibraryUploader\Http\Controllers\MediaLibraryUploaderController;
use Illuminate\Support\Facades\Route;

class RouteMixin
{
    public function mediaLibraryUploader(): Closure
    {
        return function () {
            Route::group(
                [
                    'prefix' => 'media-library-uploader',
                    'as' => 'media-library-uploader.',
                ],
                function () {
//                    $controller = config('media-library-uploader.controller');
                    Route::patch('order', [MediaLibraryUploaderController::class, 'order'])->name('order');

                    Route::post('upload/{routeKeyValue}', [MediaLibraryUploaderController::class, 'upload'])->name(
                        'upload'
                    );
//                    Route::post('multi-upload/{routeKeyValue}',  [MediaLibraryUploader::class, 'multiUpload'])->name('multi-upload');
                    Route::patch('{media}/property', [MediaLibraryUploaderController::class, 'updateProperty'])->name(
                        'update.property'
                    );
                    Route::delete('{media}', [MediaLibraryUploaderController::class, 'destroy'])->name('destroy');
                }
            );
        };
    }
}
