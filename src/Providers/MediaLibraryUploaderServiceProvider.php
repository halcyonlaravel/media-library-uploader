<?php

namespace HalcyonLaravelBoilerplate\MediaLibraryUploader\Providers;

use HalcyonLaravelBoilerplate\MediaLibraryUploader\Providers\Macros\RouteMixin;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

class MediaLibraryUploaderServiceProvider extends PackageServiceProvider
{
    protected function getPackageBaseDir(): string
    {
        return __DIR__.DIRECTORY_SEPARATOR.'..';
    }

    public function configurePackage(Package $package): void
    {
        $package
            ->name('media-library-uploader')
            ->hasConfigFile();
    }

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function packageRegistered()
    {
        $this->app->make('router')->mixin(new RouteMixin());
    }
}
