<?php

namespace HalcyonLaravelBoilerplate\MediaLibraryUploader\Http\Controllers;

use HalcyonLaravelBoilerplate\MediaLibraryUploader\MediaLibraryUploader;
use HalcyonLaravelBoilerplate\MediaLibraryUploader\Models\Media;
use HalcyonLaravelBoilerplate\MediaLibraryUploader\Models\Traits\MediaLibraryUploaderTrait;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Validation\Rule;
use InvalidArgumentException;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\MediaCollections\MediaRepository;

/**
 * Class MediaLibraryUploaderController
 *
 * @package HalcyonLaravelBoilerplate\MediaLibraryUploader\Http\Controllers
 */
class MediaLibraryUploaderController extends Controller
{
    use ValidatesRequests;

    /**
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function order(Request $request): JsonResponse
    {
        $requestData = $this->validate(
            $request,
            [
                'orderedIds.*' => [
                    'required',
                    Rule::exists(Media::class, (new Media())->getKeyName()),
                ],
            ]
        );

        config('media-library.media_model')::setNewOrder($requestData['orderedIds']);

        return response()->json(
            [
                'status'  => 200,
                'message' => 'done order',
            ]
        );
    }

    /**
     * @throws \Exception
     */
    public function destroy(string $media): JsonResponse
    {
        /** @var Media $media */
        $media = app(MediaRepository::class)->getByIds([$media])->firstOrFail();

        /** @var HasMedia|\Illuminate\Database\Eloquent\Model $model */
        $model = $media->model;

        if ($model->getMedia($media->collection_name)->count() > 1) {
            return $this->processDelete($media);
        }

        $modelClassName = $model->getMorphClass();

        if (
            array_key_exists($modelClassName, $this->noneRequiredModels()) &&
            (
                $this->noneRequiredModels()[$modelClassName] == '*' OR
                in_array($media->collection_name, $this->noneRequiredModels()[$modelClassName])
            )
        ) {
            return $this->processDelete($media);
        }

        $collectionName = 'all collection names';
        if (
            array_key_exists($modelClassName, $this->noneRequiredModels()) &&
            $this->noneRequiredModels()[$modelClassName] != '*'
        ) {
            $collectionName = "collection name [$media->collection_name].";
        }

        return response()->json(
            [
                'status' => 422,
                'message' => "Image required in [$modelClassName] with [$collectionName].",
            ],
            422
        );
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $routeKeyValue
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig|\Spatie\MediaLibrary\MediaCollections\Exceptions\FileCannotBeAdded
     */
    public function upload(Request $request, string $routeKeyValue): JsonResponse
    {
        $requestData = $this->validate(
            $request,
            [
                'model' => 'required|in:'.implode(',', array_keys($this->models())),
                'collection_name' => 'nullable|string',
                'conversion_name' => 'nullable|string',
                'is_single' => 'nullable|bool',
            ]
        );

        $model = $this->getModel($requestData['model'], $routeKeyValue);

        /** @phpstan-ignore-next-line */
        $collectionName = $requestData['collection_name'] ?? $model->collection_name ?? 'images';

        abort_if(is_null($collectionName), 422, 'No collection name specified, aborted.');

        $conversionName = $requestData['conversion_name'] ?? '';

        $rules = isset($this->validations()[$this->models()[$requestData['model']]])
            ? $this->validations()[$this->models()[$requestData['model']]]
            : [];

        if (is_array($rules)) {
            if (!blank($rules)) {
                if (array_key_exists('*', $rules)) {
                    $rules = $rules['*'];
                } elseif (array_key_exists($collectionName, $rules)) {
                    $rules = $rules[$collectionName];
                } else {
                    $rules = null;
                }
            }
        } else {
            throw  new InvalidArgumentException('Invalid argument, it must be an array.');
        }

        /** @var \Illuminate\Http\UploadedFile $requestImage */
        $requestImage = $this->validate(
            $request,
            [
                'image' => $rules ?: 'required|image',
            ]
        )['image'];

        $media = MediaLibraryUploader::setModel($model)
            ->file($requestImage)
            ->execute($collectionName);

        $this->checkClearOtherMedia($model, $media, $collectionName, $requestData);

        return response()->json(
            [
                'status' => 200,
                'data' => $media->getForImageUploader($model->allowedProperties(), $conversionName),
                'message' => 'Success upload for ['.$requestData['model'].'] with collection name ['.$collectionName.']',
            ]
        );
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateProperty(Request $request, string $media): JsonResponse
    {
        /** @var Media $media */
        $media = app(MediaRepository::class)->getByIds([$media])->firstOrFail();

        $requestData = $this->validate(
            $request,
            [
                'properties' => 'required',
            ]
        );

        foreach ($requestData['properties'] as $key => $property) {
            $media->setCustomProperty($key, $property);
        }

        $media->save();

        // Get allowed properties
        $allowedProperties = $media->custom_properties;
        unset($allowedProperties['custom_headers']);
        unset($allowedProperties['generated_conversions']);

        $properties = $media->formatCustomProperties(array_keys($allowedProperties));

        return response()->json(
            [
                'status' => 200,
                'data' => $properties,
            ]
        );
    }

//    /**
//     * @return array<string, string>
//     */
    private function models(): array
    {
        return config('media-library-uploader.api_validations.models');
    }

//    /**
//     * @return array<string, string>
//     */
    private function noneRequiredModels(): array
    {
        return config('media-library-uploader.api_validations.model_none_required');
    }

//    /**
//     * @return array<string, string>
//     */
    private function validations(): array
    {
        return config('media-library-uploader.api_validations.model_validations');
    }

    /**
     * @param  \HalcyonLaravelBoilerplate\MediaLibraryUploader\Models\Media  $media
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    private function processDelete(Media $media): JsonResponse
    {
        $media->delete();
        return response()->json([], 204);
    }

    /**
     * @param  string  $modelName
     * @param  string  $routeKeyValue
     *
     * @return \Spatie\MediaLibrary\HasMedia|\HalcyonLaravelBoilerplate\MediaLibraryUploader\Models\Traits\MediaLibraryUploaderTrait|\Illuminate\Database\Eloquent\Model
     */
    private function getModel(string $modelName, string $routeKeyValue): HasMedia
    {
        /** @var \Illuminate\Database\Eloquent\Model $model */
        $model = app($this->models()[$modelName]);

        if (
            ! in_array(MediaLibraryUploaderTrait::class, class_uses_recursive($model)) ||
            ! $model instanceof HasMedia
        ) {
            throw new InvalidArgumentException(
                'Model ['.get_class($model).'] must use '.MediaLibraryUploaderTrait::class.
                ' and implements '.HasMedia::class
            );
        }

        return $model::where($model->getRouteKeyName(), $routeKeyValue)->firstOrFail();
    }

    /**
     * @param  \Spatie\MediaLibrary\HasMedia  $model
     * @param  \HalcyonLaravelBoilerplate\MediaLibraryUploader\Models\Media  $media
     * @param  string  $collectionName
     * @param  array  $requestData
     */
    private function checkClearOtherMedia(HasMedia $model, Media $media, string $collectionName, array $requestData)
    {
        $isSingle = isset($requestData['is_single'])
            ? ((bool) $requestData['is_single'])
            : null;

        if ($isSingle === true) {
            $model->clearMediaCollectionExcept($collectionName, $media);
        }
    }
}
