<?php

namespace HalcyonLaravelBoilerplate\MediaLibraryUploader\Models\Traits;

use HalcyonLaravelBoilerplate\MediaLibraryUploader\Models\Media;
use Spatie\MediaLibrary\InteractsWithMedia;

trait MediaLibraryUploaderTrait
{
    use InteractsWithMedia;

    public function getUploaderImages(string $collectionName = 'images', string $conversionName = '')
    {
        return $this->getMedia($collectionName)->map(
            fn(Media $media) => $media->getForImageUploader($this->allowedProperties(), $conversionName)
        );
    }

    public function allowedProperties(): array
    {
        $defaultAllowedProperties = ["html_attributes"];

        $allowedProperties = defined('self::MEDIA_LIBRARY_CUSTOM_PROPERTIES')
            ? self::MEDIA_LIBRARY_CUSTOM_PROPERTIES
            : [];
        return array_merge($allowedProperties, $defaultAllowedProperties);
    }

    public function isMediaCollectionMultipleFiles(string $collectionName = ''): bool
    {
        $this->registerMediaCollections();

        return ! collect($this->mediaCollections)
            ->firstWhere('name', $collectionName)
            ->singleFile;
    }
}
