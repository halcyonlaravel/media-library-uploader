<?php

namespace HalcyonLaravelBoilerplate\MediaLibraryUploader\Models;

use Illuminate\Database\Eloquent\Builder;
use StdClass;

/**
 * HalcyonLaravelBoilerplate\MediaLibraryUploader\Models\Media
 *
 * @property int $id
 * @property string $model_type
 * @property int $model_id
 * @property string $collection_name
 * @property string $name
 * @property string $file_name
 * @property string|null $mime_type
 * @property string $disk
 * @property int $size
 * @property array $manipulations
 * @property array $custom_properties
 * @property array $responsive_images
 * @property int|null $order_column
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $extension
 * @property-read mixed $human_readable_size
 * @property-read mixed $type
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $model
 * @method static Builder|Media newModelQuery()
 * @method static Builder|Media newQuery()
 * @method static Builder|\Spatie\MediaLibrary\MediaCollections\Models\Media ordered()
 * @method static Builder|Media query()
 * @method static Builder|Media whereCollectionName($value)
 * @method static Builder|Media whereCreatedAt($value)
 * @method static Builder|Media whereCustomProperties($value)
 * @method static Builder|Media whereDisk($value)
 * @method static Builder|Media whereFileName($value)
 * @method static Builder|Media whereId($value)
 * @method static Builder|Media whereManipulations($value)
 * @method static Builder|Media whereMimeType($value)
 * @method static Builder|Media whereModelId($value)
 * @method static Builder|Media whereModelType($value)
 * @method static Builder|Media whereName($value)
 * @method static Builder|Media whereOrderColumn($value)
 * @method static Builder|Media whereResponsiveImages($value)
 * @method static Builder|Media whereSize($value)
 * @method static Builder|Media whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Media extends \Spatie\MediaLibrary\MediaCollections\Models\Media
{
    public function getForImageUploader($allowedProperties = [], string $conversionName = ''): StdClass
    {
        $obj = new StdClass;

        $obj->id = $this->id;
        $obj->name = $this->file_name;
        $obj->source = $this->getUrl($conversionName);
        $obj->thumbnail = $this->getUrl();
        $obj->deleteUrl = route('backend.media-library-uploader.destroy', $this);
        $obj->updatePropertyUrl = route('backend.media-library-uploader.update.property', $this);
        $obj->properties = $this->formatCustomProperties($allowedProperties);

        return $obj;
    }

    /**
     * @param  array  $allowedProperties
     *
     * @return \StdClass
     */
    public function formatCustomProperties(array $allowedProperties): StdClass
    {
        $customProperties = $this->custom_properties;
        $properties = new StdClass;

        foreach ($allowedProperties as $property) {
            $properties->{$property} = $customProperties[$property] ?? '';
        }

        return $properties;
    }

}
